//=================================================================================================
// Section PNG Saver
//=================================================================================================
#include "pngsave.h"
#include <cstdio>
#include <cstdlib>
#include <string>

/**
 * @brief PNG形式のファイルを読み込む。
 *
 * @param[in] fp ファイルストリーム
 * @return 読み込んだ画像、読み込みに失敗した場合NULL
 */
image_t *read_png_stream(FILE *fp) {
  image_t *img = NULL;
  int i, x, y;
  int width, height;
  int num;
  png_colorp palette;
  png_structp png = NULL;
  png_infop info = NULL;
  png_bytep row;
  png_bytepp rows;
  png_byte sig_bytes[8];
  if (fread(sig_bytes, sizeof(sig_bytes), 1, fp) != 1) {
    return NULL;
  }
  if (png_sig_cmp(sig_bytes, 0, sizeof(sig_bytes))) {
    return NULL;
  }
  png = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  if (png == NULL) {
    goto error;
  }
  info = png_create_info_struct(png);
  if (info == NULL) {
    goto error;
  }
  if (setjmp(png_jmpbuf(png))) {
    goto error;
  }
  png_init_io(png, fp);
  png_set_sig_bytes(png, sizeof(sig_bytes));
  png_read_png(png, info, PNG_TRANSFORM_PACKING | PNG_TRANSFORM_STRIP_16, NULL);
  width = png_get_image_width(png, info);
  height = png_get_image_height(png, info);
  rows = png_get_rows(png, info);
  // 画像形式に応じて詰め込み
  switch (png_get_color_type(png, info)) {
    case PNG_COLOR_TYPE_PALETTE:  // インデックスカラー
      if ((img = allocate_image(width, height, COLOR_TYPE_INDEX)) == NULL) {
        goto error;
      }
      png_get_PLTE(png, info, &palette, &num);
      img->palette_num = num;
      for (i = 0; i < num; i++) {
        png_color pc = palette[i];
        img->palette[i] = color_from_rgb(pc.red, pc.green, pc.blue);
      }
      {
        png_bytep trans = NULL;
        int num_trans = 0;
        if (png_get_tRNS(png, info, &trans, &num_trans, NULL) == PNG_INFO_tRNS
            && trans != NULL && num_trans > 0) {
          for (i = 0; i < num_trans; i++) {
            img->palette[i].a = trans[i];
          }
          for (; i < num; i++) {
            img->palette[i].a = 0xff;
          }
        }
      }
      for (y = 0; y < height; y++) {
        row = rows[y];
        for (x = 0; x < width; x++) {
          img->map[y][x].i = *row++;
        }
      }
      break;
    case PNG_COLOR_TYPE_GRAY:  // グレースケール
      if ((img = allocate_image(width, height, COLOR_TYPE_GRAY)) == NULL) {
        goto error;
      }
      for (y = 0; y < height; y++) {
        row = rows[y];
        for (x = 0; x < width; x++) {
          img->map[y][x].g = *row++;
        }
      }
      break;
    case PNG_COLOR_TYPE_GRAY_ALPHA:  // グレースケール+α
      if ((img = allocate_image(width, height, COLOR_TYPE_RGBA)) == NULL) {
        goto error;
      }
      for (y = 0; y < height; y++) {
        row = rows[y];
        for (x = 0; x < width; x++) {
          u8 g = *row++;
          img->map[y][x].c.r = g;
          img->map[y][x].c.g = g;
          img->map[y][x].c.b = g;
          img->map[y][x].c.a = *row++;
        }
      }
      break;
    case PNG_COLOR_TYPE_RGB:  // RGB
      if ((img = allocate_image(width, height, COLOR_TYPE_RGB)) == NULL) {
        goto error;
      }
      for (y = 0; y < height; y++) {
        row = rows[y];
        for (x = 0; x < width; x++) {
          img->map[y][x].c.r = *row++;
          img->map[y][x].c.g = *row++;
          img->map[y][x].c.b = *row++;
          img->map[y][x].c.a = 0xff;
        }
      }
      break;
    case PNG_COLOR_TYPE_RGB_ALPHA:  // RGBA
      if ((img = allocate_image(width, height, COLOR_TYPE_RGBA)) == NULL) {
        goto error;
      }
      for (y = 0; y < height; y++) {
        row = rows[y];
        for (x = 0; x < width; x++) {
          img->map[y][x].c.r = *row++;
          img->map[y][x].c.g = *row++;
          img->map[y][x].c.b = *row++;
          img->map[y][x].c.a = *row++;
        }
      }
      break;
  }
  error:
  png_destroy_read_struct(&png, &info, NULL);
  return img;
}

/**
 * @brief PNG形式のファイルを読み込む。
 *
 * @param[in] filename ファイル名
 * @return 読み込んだ画像、読み込みに失敗した場合NULL
 */
image_t *read_png_file(const char *filename) {
  FILE *fp = fopen(filename, "rb");
  if (fp == NULL) {
    perror(filename);
    return NULL;
  }
  image_t *img = read_png_stream(fp);
  fclose(fp);
  return img;
}

/**
 * @brief PNG形式としてファイルに書き出す。
 *
 * @param[in] fp  書き出すファイルストリームのポインタ
 * @param[in] img 画像データ
 * @return 成否
 */
result_t write_png_stream(FILE *fp, image_t *img) {
	int i, x, y;
	result_t result = FAILURE;
	int row_size;
	int color_type;
	png_structp png = NULL;
	png_infop info = NULL;
	png_bytep row;
	png_bytepp rows = NULL;
	png_colorp palette = NULL;
	if (img == NULL) {
		return result;
	}
	switch (img->color_type) {
		case COLOR_TYPE_INDEX:  // インデックスカラー
			color_type = PNG_COLOR_TYPE_PALETTE;
			row_size = sizeof(png_byte) * img->width;
			break;
		case COLOR_TYPE_GRAY:  // グレースケール
			color_type = PNG_COLOR_TYPE_GRAY;
			row_size = sizeof(png_byte) * img->width;
			break;
		case COLOR_TYPE_RGB:  // RGB
			color_type = PNG_COLOR_TYPE_RGB;
			row_size = sizeof(png_byte) * img->width * 3;
			break;
		case COLOR_TYPE_RGBA:  // RGBA
			color_type = PNG_COLOR_TYPE_RGBA;
			row_size = sizeof(png_byte) * img->width * 4;
			break;
		default:
			return FAILURE;
	}
	png = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (png == NULL) {
		goto error;
	}
	info = png_create_info_struct(png);
	if (info == NULL) {
		goto error;
	}
	if (setjmp(png_jmpbuf(png))) {
		goto error;
	}
	png_init_io(png, fp);
	png_set_IHDR(png, info, img->width, img->height, 8,
				 color_type, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT,
				 PNG_FILTER_TYPE_DEFAULT);
	rows = (png_bytepp)png_malloc(png, sizeof(png_bytep) * img->height);
	if (rows == NULL) {
		goto error;
	}
	png_set_rows(png, info, rows);
	memset(rows, 0, sizeof(png_bytep) * img->height);
	for (y = 0; y < img->height; y++) {
		if ((rows[y] = (png_bytep)png_malloc(png, row_size)) == NULL) {
			goto error;
		}
	}
	switch (img->color_type) {
		case COLOR_TYPE_INDEX:  // インデックスカラー
			palette = (png_colorp)png_malloc(png, sizeof(png_color) * img->palette_num);
			for (i = 0; i < img->palette_num; i++) {
				palette[i].red = img->palette[i].r;
				palette[i].green = img->palette[i].g;
				palette[i].blue = img->palette[i].b;
			}
			png_set_PLTE(png, info, palette, img->palette_num);
			for (i = img->palette_num - 1; i >= 0 && img->palette[i].a != 0xff; i--);
			if (i >= 0) {
				int num_trans = i + 1;
				png_byte trans[255];
				for (i = 0; i < num_trans; i++) {
					trans[i] = img->palette[i].a;
				}
				png_set_tRNS(png, info, trans, num_trans, NULL);
			}
			png_free(png, palette);
			for (y = 0; y < img->height; y++) {
				row = rows[y];
				for (x = 0; x < img->width; x++) {
					*row++ = img->map[y][x].i;
				}
			}
			break;
		case COLOR_TYPE_GRAY:  // グレースケール
			for (y = 0; y < img->height; y++) {
				row = rows[y];
				for (x = 0; x < img->width; x++) {
					*row++ = img->map[y][x].g;
				}
			}
			break;
		case COLOR_TYPE_RGB:  // RGB
			for (y = 0; y < img->height; y++) {
				row = rows[y];
				for (x = 0; x < img->width; x++) {
					*row++ = img->map[y][x].c.r;
					*row++ = img->map[y][x].c.g;
					*row++ = img->map[y][x].c.b;
				}
			}
			break;
		case COLOR_TYPE_RGBA:  // RGBA
			for (y = 0; y < img->height; y++) {
				row = rows[y];
				for (x = 0; x < img->width; x++) {
					*row++ = img->map[y][x].c.r;
					*row++ = img->map[y][x].c.g;
					*row++ = img->map[y][x].c.b;
					*row++ = img->map[y][x].c.a;
				}
			}
			break;
	}
	png_write_png(png, info, PNG_TRANSFORM_IDENTITY, NULL);
	result = SUCCESS;
  error:
	if (rows != NULL) {
		for (y = 0; y < img->height; y++) {
			png_free(png, rows[y]);
		}
		png_free(png, rows);
	}
	png_destroy_write_struct(&png, &info);
	return result;
}

/**
 * @brief PNG形式としてファイルに書き出す。
 *
 * @param[in] filename 書き出すファイル名
 * @param[in] img      画像データ
 * @return 成否
 */
result_t write_png_file(const char *filename, image_t *img) {
	result_t result = FAILURE;
	if (img == NULL) {
		return result;
	}
	FILE *fp = fopen(filename, "wb");
	if (fp == NULL) {
		perror(filename);
		return result;
	}
	result = write_png_stream(fp, img);
	fclose(fp);
	return result;
}

/**
 * @brief image_t型構造体のメモリを確保し初期化する。
 *
 * @param[in] width   画像の幅
 * @param[in] height  画像の高さ
 * @param[in] type    色表現の種別
 * @return 初期化済みimage_t型構造体
 */
image_t *allocate_image(u32 width, u32 height, u8 type) {
  u32 i;
  image_t *img;
  if ((img = static_cast<image_t *>(std::calloc(1, sizeof(image_t)))) == NULL) {
    return NULL;
  }
  img->width = width;
  img->height = height;
  img->color_type = type;
  if (type == COLOR_TYPE_INDEX) {
    if ((img->palette = static_cast<color_t *>(std::calloc(256, sizeof(color_t)))) == NULL) {
      goto error;
    }
  } else {
    img->palette = NULL;
  }
  img->palette_num = 0;
  if ((img->map = static_cast<pixcel_t **>(std::calloc(height, sizeof(pixcel_t*)))) == NULL) {
    goto error;
  }
  for (i = 0; i < height; i++) {
    if ((img->map[i] = static_cast<pixcel_t *>(std::calloc(width, sizeof(pixcel_t)))) == NULL) {
      goto error;
    }
  }
  return img;
  error:
  free_image(img);
  return NULL;
}

/**
 * @brief image_t型のクローンを作成する。
 *
 * カラーパレットやイメージデータは内部的に別にメモリを確保しているため、
 * allocateした後にmemcpyしてもクローンは作成できない。
 * この関数を使ってdeepcopyを行うこと。
 *
 * @param[in] img クローン元image_t型構造体
 * @return クローンされたimage_t型構造体
 */
image_t *clone_image(image_t *img) {
  u32 i;
  image_t *new_img = allocate_image(img->width, img->height, img->color_type);
  if (new_img == NULL) {
    return NULL;
  }
  new_img->palette_num = img->palette_num;
  if (img->color_type == COLOR_TYPE_INDEX) {
    memcpy(new_img->palette, img->palette, sizeof(color_t) * img->palette_num);
  }
  for (i = 0; i < img->height; i++) {
    memcpy(new_img->map[i], img->map[i], sizeof(pixcel_t) * img->width);
  }
  return new_img;
}

/**
 * @brief image_t型構造体のメモリを開放する。
 *
 * 内部的に確保したメモリも開放する。
 * 内部メンバーのポインタを直接変更した場合
 * 正常に動作しないため注意。
 *
 * @param[in,out] img 開放するimage_t型構造体
 */
void free_image(image_t *img) {
  u32 i;
  if (img == NULL) {
    return;
  }
  if (img->palette != NULL) {
    std::free(img->palette);
  }
  for (i = 0; i < img->height; i++) {
    std::free(img->map[i]);
  }
  std::free(img->map);
  std::free(img);
}

/**
 * @brief RGB値を指定してcolor_t型の値を作成する。
 *
 * アルファ値は0xffが設定される。
 *
 * @param[in] r red
 * @param[in] g green
 * @param[in] b blue
 * @return color_t
 */
color_t color_from_rgb(u8 r, u8 g, u8 b) {
  color_t c;
  c.r = r;
  c.g = g;
  c.b = b;
  c.a = 0xff;
  return c;
}

//=================================================================================================
//=================================================================================================
//=================================================================================================
