//=================================================================================================
// HQW Arguments Analizer
//=================================================================================================
#include <cstdio>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <regex>
#include <algorithm>

#include <sys/stat.h>
#include <dirent.h>

//#define __Cygwin64
#ifdef __Cygwin64
#include <stdlib.h>  //strtol strtof
#endif

//=================================================================================================
// Name Space
//=================================================================================================
namespace hqw {
namespace args {

//-------------------------------------------------------------------------------------------------
// Option Element
//-------------------------------------------------------------------------------------------------
struct Option
{
	bool        isSet;
	enum ValueType
	{
		vNon,
		vBool,
		vInteger,
		vString,
		vFloat,
		vIntArray,
	}			vType;
	bool        bValue;
	int         iValue;
	std::string sValue;
	float       fValue;
	std::vector<int> iaValue;

	Option() : isSet(false), vType(vNon) {}
	Option(bool _default) : isSet(false), vType(vBool) {
		bValue = _default;
	}
	Option(int _default) : isSet(false), vType(vInteger) {
		iValue = _default;
	}
	Option(std::vector<int>& _default) : isSet(false), vType(vIntArray) {
		iaValue = _default;
	}
	Option(const char* _default) : isSet(false), vType(vString) {
		sValue = _default;
	}
	Option(float _default) : isSet(false), vType(vFloat) {
		fValue = _default;
	}
	Option* Key() {
		if (vType == vBool) {
			if (!isSet) {
				bValue = !bValue;
				isSet = true;
			}
			return NULL;
		}
		else {
			return this;
		}
	}
	Option* Param(const std::string param) {
		switch (vType) {
			case vInteger:
#ifdef __Cygwin64
				iValue = ::strtol(param.c_str(),NULL,10);
#else
				iValue = std::stoi(param);
#endif
				isSet = true;
				break;
			case vIntArray:
				{
					int cutAt;
					std::string s = param;
					while( (cutAt = s.find_first_of(",")) != s.npos )
					{
						if(cutAt > 0)
						{
							iaValue.push_back( std::stoi(s.substr(0, cutAt)) );
						}
						s = s.substr(cutAt + 1);
					}
					if(s.length() > 0) {
						iaValue.push_back( std::stoi(s) );
					}
					/*
					std::stringstream ss(param);
					string s;
					while (getline(ss, s, ',')) {
						iaValue.push_back( std::stoi(s) );
					}
					*/
				}
				isSet = true;
				break;
			case vString:
				sValue = param;
				isSet = true;
				break;
			case vFloat:
#ifdef __Cygwin64
				fValue = ::strtof(param.c_str(),NULL);
#else
				fValue = std::stof(param);
#endif
				isSet = true;
				break;
			case vBool:
			default:
				return NULL;
		}
		return NULL;
	}

	bool Check() {
		if (vType == vBool) {
			return bValue;
		}
		else {
			return isSet;
		}
	}
	//! @brief	Accessor
	operator const int() {
		if (vType == vInteger) {
			return iValue;
		}
		else if (vType == vFloat) {
			return static_cast<int>(fValue);
		}
		else {
			return 0;
		}
	}
	operator const std::string() {
		if (vType == vString) {
			return sValue;
		}
		else {
			return "";
		}
	}
	operator const float() {
		if (vType == vInteger) {
			return static_cast<float>(iValue);
		}
		else if (vType == vFloat) {
			return fValue;
		}
		else {
			return 0;
		}
	}
	operator const std::vector<int>() {
		return iaValue;
	}
};

typedef std::map<std::string,Option> Options;

//-------------------------------------------------------------------------------------------------
// Functions
//-------------------------------------------------------------------------------------------------
// Arguments Analise to files & options
void Analyzer(int argc, char* argv[], std::vector<std::string>& filelist, Options& options);

// Create Out Stream
std::ostream* CreateOutStream(std::string outpath, std::string outname, std::string& outfile);


}	// end of namespace(args)
}	// end of namespace(hqw)
//=================================================================================================
//=================================================================================================
//=================================================================================================
