//=================================================================================================
// HQW Arguments Analizer
//=================================================================================================
#include "libarg.h"

//=================================================================================================
// Name Space
//=================================================================================================
namespace hqw {
namespace args {

using namespace std;

//-------------------------------------------------------------------------------------------------
// Arguments Analise to files & options
//-------------------------------------------------------------------------------------------------
void Analyzer(int argc, char* argv[], std::vector<std::string>& filelist, Options& options)
{
	Option* checked = NULL;
	for (int ccarg = 1; ccarg < argc; ccarg++) {
		std::string argpath = argv[ccarg];

		// Check Options
		if (options.find(argpath) != options.end()) {
			checked = options[argpath].Key();
		}
		else if (checked != NULL) {
			checked = checked->Param(argpath);
		}

		// Wild card ... (mac/linux is irrelevant)
		else if (argpath.find('*') != std::string::npos) {
			std::regex reass("\\*");
			std::regex redot("\\.");
			if (std::regex_match(argpath, reass )) {
				std::regex_replace(argpath, reass, ".*") ;
			}
			if (std::regex_match(argpath, redot )) {
				std::regex_replace(argpath, redot, "\\.") ;
			}

			std::regex re(argpath);   //"k.*\\.csv"

			DIR *dp = opendir(".");
			struct dirent *dirst;
			while ((dirst = readdir(dp)) != NULL)
			{
				std::string filepath = dirst->d_name;
				if (!std::regex_match(filepath, re)) {
					continue;
				}
				//std::cout << filepath << std::endl;
				filelist.push_back(filepath);
			}
			closedir(dp);
		}
		else {
			filelist.push_back(argpath);
		}
	}
}

//-------------------------------------------------------------------------------------------------
// Create Out Stream
//-------------------------------------------------------------------------------------------------
std::ostream* CreateOutStream(std::string outpath, std::string outname, std::string& outfile)
{
	if (std::regex_match( outpath, std::regex(".*/$"))) {
		outfile = outpath + outname;
	} else {
		outfile = outpath;
	}
	return new std::ofstream( outfile, std::ios::out );
}

}	// end of namespace(args)
}	// end of namespace(hqw)
//=================================================================================================
//=================================================================================================
//=================================================================================================
