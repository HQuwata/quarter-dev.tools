//=================================================================================================
// Section PNG Saver
//=================================================================================================
#include <png.h>

#define COLOR_TYPE_INDEX 0   /**< インデックスカラー方式 */
#define COLOR_TYPE_GRAY  1   /**< グレースケール方式 */
#define COLOR_TYPE_RGB   2   /**< RGB方式 */
#define COLOR_TYPE_RGBA  3   /**< RGBA方式 */

#define	u8	unsigned char
#define	u16	unsigned short
#define	u32	unsigned int

/**
 * @brief 色情報
 *
 * RGBAの色情報を保持する構造体
 */
struct color_t {
	u8 r; /**< Red */
	u8 g; /**< Green */
	u8 b; /**< Blue */
	u8 a; /**< Alpha */
};

/**
 * @brief 画素情報
 *
 * 共用体になっており、
 * RGBA値、グレースケール、カラーインデックス、のいずれかを表現する。
 * 単体ではどの表現形式になっているかを判断することはできない。
 */
typedef union pixcel_t {
	color_t c; /**< RGBA */
	u8 g; /**< グレースケール */
	u8 i; /**< カラーインデックス */
} pixcel_t;

/**
 * @brief 画像データ保持の構造体
 *
 * 画像データとして保持するために必要最小限の情報を格納する。
 * 各種メタデータの保持については今後の課題。
 *
 * 画素情報については、ポインタのポインタで表現しており
 * 各行へのポインタを保持する配列へのポインタとなっている。
 */
typedef struct image_t {
	u32 width;       /**< 幅 */
	u32 height;      /**< 高さ */
	u16 color_type;  /**< 色表現の種別 */
	u16 palette_num; /**< カラーパレットの数 */
	color_t *palette;     /**< カラーパレットへのポインタ */
	pixcel_t **map;       /**< 画像データ */
} image_t;

#define TRUE  1
#define FALSE 0
typedef enum result_t {
  SUCCESS = 0, /**< 成功 */
  FAILURE = -1, /**< 失敗 */
} result_t;

image_t *read_png_file(const char *filename);
result_t write_png_file(const char *filename, image_t *img);

image_t *allocate_image(u32 width, u32 height, u8 type);
void free_image(image_t *img);
color_t color_from_rgb(u8 r, u8 g, u8 b);

//=================================================================================================
//=================================================================================================
//=================================================================================================



