//=================================================================================================
// Name:	png2win.cpp
// Thema:		Convert .png to .img
// History:		2016.10.6	H.Kuwata
//=================================================================================================
//=================================================================================================
// References
//=================================================================================================
#include <cstdio>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <regex>
#include <sys/stat.h>
#include <dirent.h>

#define __Cygwin64
#ifdef __Cygwin64
#include <stdlib.h>  //strtol strtof
#endif

#include "libarg.h"
#include "pngsave.h"

//#define __Debug_Dump

//=================================================================================================
// Definition
//=================================================================================================
using namespace std;

#define	byte	unsigned char
#define	word	unsigned short
#define	sbyte	char
#define	sword	short
#define	ubyte	unsigned char
#define	uword	unsigned short

//=================================================================================================
// Section Palette
//=================================================================================================
int  Palette_num = 0;
byte Palette[48];

//=================================================================================================
// Section PICT
//=================================================================================================

//=================================================================================================
// Section PNG Converter
//=================================================================================================
static	const int	_plane	=	32768;
byte vram[_plane*4];
byte *plane_b = &vram[0];
byte *plane_r = &vram[_plane];
byte *plane_g = &vram[_plane*2];
byte *plane_h = &vram[_plane*3];

static	byte	dbuf[65536*2];
static	byte	analog_buf[2048];//32+48

struct BitMap
{
	byte	m_data[32768];
	byte	m_mask;
	int		m_index;

	void reset()
	{
		std::memset(m_data, 0, sizeof(m_data));
		m_mask = 1;
		m_index  = 0 - 1;
	}
	void writeBitmap(bool f)
	{
		m_mask >>= 1;
		if ( m_mask == 0 ) {
#if defined( __Debug_Dump )
			std::cout <<  "Bitmap[" << std::hex << "0x" << m_index << "]:" << "0x"  << (int)(m_data[m_index]&0xff) <<std::dec << std::endl;
#endif
			m_mask = 0x080;
			m_index ++;
		}
		if (f) m_data[m_index] |= m_mask;
	}
	int getTotalSize()
	{
		return m_index + 1;//(m_mask != 0 ? 1 : 0);
	}
};
static	BitMap	bitmap;

static	byte	compress[32768];

class Png2Win
{
	const std::string rootpath;
	const std::string outpath;
	const std::string palpath;
	bool        verbose;

	bool  telop_mode;
	bool  analog_add;
	int   press_mode;

	bool  united_mode;
	std::string outtag;

	byte  *dbp;

	byte   cur_data;
	int    run_length;
	byte   LH;		// L/H select flag
	byte  *cbp;		// compress ptr

	std::ostream* _ofp;

public:
	Png2Win(hqw::args::Options& options)
		: rootpath( options["-i"] )
		, outpath( options["-o"] )
		, verbose( options["-v"].Check() )
		, telop_mode( options["-telop"].Check() )
		, analog_add( options["-analog"].Check() )
		, press_mode( options["-c"] )
		, united_mode(false)
		, _ofp(NULL)
	{
		// united output
		if (std::regex_match( outpath, std::regex("^.*\\.[a-z0-9A-Z_]+$"))) {
			united_mode = true;
		}
		// Reset Data Ptr
		dbp = dbuf;
	}
	~Png2Win() {
		if (_ofp != NULL) delete(_ofp);
	}
	bool IsUniteMode() { return united_mode; }

	void SaveFile()
	{
		std::string outfile = (united_mode ? outpath : outtag + ".img");

		if (dbp != dbuf) {
			if (verbose) {
				std::cout << ("Out File "+outfile) << std::endl;
			}

			// Open File
			FILE *fp = fopen(outfile.c_str(), "wb");
			if (fp == NULL) {
				std::cout << "Save Error!! " << outfile << std::endl;
				exit(0);
			}
			// Append Analog Palette Table
			if (analog_add && Palette_num != 0) {
				static const char  APH[32+1] = {"Analog Pallette V16.1   By KAME."};
				fwrite(APH, 1, 32, fp);
				fwrite(Palette, 1, 48, fp);
			}
			// Image Save
			fwrite(dbuf, 1, (dbp - dbuf), fp);
			// Close File
			fclose(fp);
		}

		// Reset Data Ptr
		dbp = dbuf;
	}

	void ScanFile(const std::string filepath)
	{
		// Output name from src file name.
		outtag = filepath;
		outtag = std::regex_replace( outtag, std::regex("^.*/"), "");
		outtag = std::regex_replace( outtag, std::regex("\\..*$"), "");
		if (std::regex_match( outpath, std::regex(".*/$"))) {
			std::transform(outtag.begin(), outtag.end(), outtag.begin(), ::tolower);
			outtag = outpath + outtag;
		}
		// Src file path
		std::string srcfile = rootpath+filepath;
		if (verbose) {
			std::cout << ("Src File "+srcfile) << std::endl;
		}

		// Load PNG
		image_t *_png = read_png_file(srcfile.c_str());
		if (!_png) {
			std::cout << "Load Error!! " << srcfile << std::endl;
			exit(0);
		}
		image_t &png = *_png;

		// Index Color Only
		if (png.color_type != COLOR_TYPE_INDEX || png.palette_num == 0 /*|| png.palette_num > 16*/) {
			std::cout << "Index 16 Color PNG Only!! " << srcfile << std::endl;
			exit(0);
		}

		// Palette Load
		if (verbose) {
			std::cout << "Palette " << png.palette_num << std::endl;
		}
		Palette_num = (png.palette_num > 16 ? 16 : png.palette_num);
		std::memset(Palette, 0, sizeof(Palette));
		for (int p = 0; p < Palette_num; p++) {
			color_t &pp = png.palette[p];
			byte b = pp.b >> 4;
			byte r = pp.r >> 4;
			byte g = pp.g >> 4;
			Palette[p*3+0] = b | (b<<4);
			Palette[p*3+1] = r | (r<<4);
			Palette[p*3+2] = g | (g<<4);
		}

		// Pict Header 16 bytes
		if (verbose) {
			std::cout << "Size " << png.width << "x" << png.height << std::endl;
		}

		// Image Expand
		{
			int pindex = 0;
			for (byte plane = 0x01; plane < 0x10; plane <<= 1) {
				byte *vdp = &vram[_plane * pindex];
				for (int y = 0; y < png.height; y++) {
					pixcel_t* row = png.map[y];
					for (int x = 0; x < png.width/8; x++) {
						byte pixel = 0;
						for (int b = 0; b < 8; b++) {
							pixel <<= 1;
							pixel |= ((row[x*8+b].i & plane) != 0 ? 1 : 0);
						}
						*vdp++ = pixel;
					}
				}
				++pindex;
			}
		}

		byte *data_top = dbp;
		{
			// 8 ATTR
			byte attr = 0;
			// 0-4 PICT
			static const char  Head[5+1] = {"PICT "};
			for (int h = 0; h < 5; h++) *dbp++ = Head[h];
			// 5 size X
			int dx = png.width / 8;
			*dbp++ = dx;
			// 6 size Y
			int dy = png.height;
			if (dy > 256) { dy -= 256; attr |= 0x80; }
			*dbp++ = (byte)dy;
			// 7 color
			*dbp++ = (telop_mode ? 1 : 15);
			// 8 attr
			*dbp++ = attr | 0x40 | press_mode;    // 0x40:Ext +7byte, 10:No Pressed
			// 9-15 0,0,8,0,0,0,0
			static const byte Rest[7] = {0,0,8,0,0,0,0};
			for (int r = 0; r < 7; r++) *dbp++ = Rest[r];
		}

		// Image Expand
		switch (press_mode)
		{
			// 6) 4x2 Bitmap
			case 6:
			{
				int datsize, bitsize;
				Press6( png, datsize, bitsize );
				dbp = writeAX( dbp, (int)(dbp-data_top) + datsize + 2 );
				for (int c = 0; c < datsize; c++) *dbp++ = compress[c];
				for (int c = 0; c < bitsize; c++) *dbp++ = bitmap.m_data[c];
				*dbp++ = 0;
				break;
			}
			// 7) 4x1 Bitmap
			case 7:
			{
				int datsize, bitsize;
				Press7( png, datsize, bitsize );
				dbp = writeAX( dbp, (int)(dbp-data_top) + datsize + 2 );
				for (int c = 0; c < datsize; c++) *dbp++ = compress[c];
				for (int c = 0; c < bitsize; c++) *dbp++ = bitmap.m_data[c];
				break;
			}
			// 10) UnCompress
			case 10:
			{
				for (int pindex = 0; pindex < 4; pindex++) {
					byte *vdp = &vram[_plane * pindex];
					for (int y = 0; y < png.height; y++) {
						for (int x = 0; x < png.width/8; x++) {
							*dbp++ = *vdp++;
						}
					}
				}
#if 0
				for (byte plane = 0x01; plane < 0x10; plane <<= 1) {
					for (int y = 0; y < png.height; y++) {
						pixcel_t* row = png.map[y];
						for (int x = 0; x < png.width/8; x++) {
							byte pixel = 0;
							for (int b = 0; b < 8; b++) {
								pixel <<= 1;
								pixel |= ((row[x*8+b].i & plane) != 0 ? 1 : 0);
							}
							*dbp++ = pixel;
						}
					}
				}
#endif
				break;
			}
			default:
			{
				std::cout << "Not Supported Press Type!! " << press_mode << std::endl;
				exit(0);
				break;
			}
		}

		free_image(_png);
	}

private:
	byte* writeLH( byte*dst, int data )
	{
		if ( LH != 0 )		// L/H select flag
		{
			// write 0x0f and next
			*dst++ |= (byte)(data & 0x0f);
		}
		else
		{
			// write 0xf0
			*dst = (byte)((data & 0x0f) << 4);
		}
		LH ^= 0x01;
		return dst;
	}
	byte* writeAL( byte*dst, int data )
	{
		*dst++ = (byte)data;
		return dst;
	}
	byte* writeAX( byte*dst, int data )
	{
		*dst++ = (byte)data;
		*dst++ = (byte)(data>>8);
		return dst;
	}

	// 6) 4x2 Bitmap
	void Press6( const image_t &png, int &datsize, int &bitsize )
	{
		// compress buffer reset
		std::memset(compress, 0, sizeof(compress));
		cur_data = 0;
		run_length = 0;
		LH = 0;		// L/H select flag
		cbp = compress;		// compress ptr
		// bitmap buffer reset
		bitmap.reset();
		// scan 4x2 Left/Right
		for (int pindex = 0; pindex < 4; pindex++) {
			byte *vdp = &vram[_plane * pindex];
			int height_y = png.height;
			int width_x = png.width/8;
			for ( int x = 0; x < width_x; x++ )
			{
				// left
				for ( int y = 0; y < height_y; y+=2 )
				{
					unsigned d0 = ((*vdp & 0xf0) >> 4);
					unsigned d1 = ((*(vdp+width_x) & 0xf0) >> 4);
					Push6( (byte)(d0 << 4 | d1) );
					vdp += width_x * 2;
				}
				// right
				for ( int y = 0; y < height_y; y+=2 )
				{
					vdp -= width_x * 2;
					unsigned d0 = (*vdp & 0x0f);
					unsigned d1 = (*(vdp+width_x) & 0x0f);
					Push6( (byte)(d0 << 4 | d1) );
				}
				vdp++;
			}
			if (run_length != 0) {
				Write6(true);
				run_length = 0;
			}
		}
		datsize = (int)(cbp - compress) + (LH != 0 ? 1 : 0);
		bitsize = bitmap.getTotalSize();
	}
	void Push6( byte data )
	{
		if ( run_length == 0 ) {
			cur_data = data;
			run_length++;
		}
		else if ( cur_data != data ) {
			Write6();
			run_length = 1;
			cur_data = data;
		}
		else {
			run_length++;
			if ( run_length >= 255 ) {
				Write6();
				run_length = 0;
			}
		}
	}
	void Write6( bool _last=false )
	{
#if defined( __Debug_Dump )
		std::cout << std::hex << "0x" << (int)cur_data <<std::dec <<".."<<run_length << std::endl;
#endif
		if ( run_length == 1 && !_last ) {
			bitmap.writeBitmap(false);
			cbp = writeLH( cbp, cur_data >> 4 );
			cbp = writeLH( cbp, cur_data );
		}
		else {
			bitmap.writeBitmap(true);
			if ( run_length <= 16 ) {
				bitmap.writeBitmap(false);
				cbp = writeLH( cbp, run_length-1 );
			}
			else {
				bitmap.writeBitmap(true);
				cbp = writeLH( cbp, run_length >> 4 );
				cbp = writeLH( cbp, run_length );
			}
			cbp = writeLH( cbp, cur_data >> 4 );
			cbp = writeLH( cbp, cur_data );
		}
	}

	// 7) 4x1 Bitmap
	void Press7( const image_t &png, int &datsize, int &bitsize )
	{
		// compress buffer reset
		std::memset(compress, 0, sizeof(compress));
		cur_data = 0;
		run_length = 0;
		LH = 0;		// L/H select flag
		cbp = compress;		// compress ptr
		// bitmap buffer reset
		bitmap.reset();
		// scan 4x1 Left/Right
		for (int pindex = 0; pindex < 4; pindex++) {
			byte *vdp = &vram[_plane * pindex];
			int height_y = png.height;
			int width_x = png.width/8;
			for ( int x = 0; x < width_x; x++ )
			{
				// left
				for ( int y = 0; y < height_y; y++ )
				{
					unsigned d0 = ((*vdp & 0xf0) >> 4);
					Push7( (byte)(d0) );
					vdp += width_x;
				}
				// right
				for ( int y = 0; y < height_y; y++ )
				{
					vdp -= width_x;
					unsigned d0 = (*vdp & 0x0f);
					Push7( (byte)(d0) );
				}
				vdp++;
			}
			if (run_length != 0) {
				Write7(true);
				run_length = 0;
			}
		}
		datsize = (int)(cbp - compress) + (LH != 0 ? 1 : 0);
		bitsize = bitmap.getTotalSize();
	}
	void Push7( byte data )
	{
		if ( run_length == 0 ) {
			cur_data = data;
			run_length++;
		}
		else if ( cur_data != data ) {
			Write7();
			run_length = 1;
			cur_data = data;
		}
		else {
			run_length++;
			if ( run_length >= 16 ) {
				Write7();
				run_length = 0;
			}
		}
	}
	void Write7( bool _last=false )
	{
#if defined( __Debug_Dump )
		std::cout << std::hex << "0x" << (int)cur_data <<std::dec <<".."<<run_length << std::endl;
#endif
		cbp = writeLH( cbp, cur_data );
		if ( run_length == 1 && !_last ) {
			bitmap.writeBitmap(false);
		}
		else {
			bitmap.writeBitmap(true);
			cbp = writeLH( cbp, run_length-1 );
		}
	}
};

//=================================================================================================
// Section Main Entry
//=================================================================================================
int main(int argc, char* argv[])
{
	if (argc < 2) {
		std::cout << "png2win [options] png-file" << std::endl;
		std::cout << " .png to .win Converter" << std::endl;
		std::cout << " options" << std::endl;
		std::cout << "  -o path|file : Path of result (ex. -o ../ or -o out/result.img)" << std::endl;
		std::cout << "  -i path : Path of src image (ex. -i ../src/ )" << std::endl;
		std::cout << "  -telop  : Out telop type image" << std::endl;
		std::cout << "  -analog : Append pallete table" << std::endl;
		std::cout << "  -c N    : Press Type 10 or 6 (default 10)" << std::endl;
		std::cout << "  -v      : Verbose" << std::endl;
		std::cout << " ex)" << std::endl;
		std::cout << "  png2win xxx.png" << std::endl;
		std::cout << "  png2win p1.png p2.png -o p.img ; p1 & p2 to p.img" << std::endl;
		std::cout << "  png2win xxx.png -telop         ; convert for telop image." << std::endl;
		std::cout << "  png2win xxx.png -analog        ; append pallete table." << std::endl;
		return 1;
	}

	std::vector<std::string> filelist;
	hqw::args::Options options;
	std::vector<int> pict_list;

	options["-v"] = false;  // Verbose
	options["-i"] = "";  // Load Path
	options["-o"] = "";  // Out Path
	options["-c"] = 10;  // Press Type

	options["-telop"] = false;
	options["-analog"] = false;

	hqw::args::Analyzer(argc, argv, filelist, options);

	Png2Win png2win(options);
	for (std::vector<std::string>::iterator itr = filelist.begin(); itr != filelist.end(); ++itr) {
		std::string filepath = (*itr);
		png2win.ScanFile(filepath);
		if (!png2win.IsUniteMode()) {
			png2win.SaveFile();
		}
	}
	if (png2win.IsUniteMode()) {
		png2win.SaveFile();
	}

    return 0;
}
