//=================================================================================================
// ファイル名:	win2png.cpp
// 主題:		win を png に変換
// 履歴:		2016.09.23	H.Kuwata	新規作成
//=================================================================================================
//=================================================================================================
// 参照するファイル
//=================================================================================================
#include <cstdio>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <regex>
#include <sys/stat.h>
#include <dirent.h>

#define __Cygwin64
#ifdef __Cygwin64
#include <stdlib.h>  //strtol strtof
#endif

#include "libarg.h"
#include "pngsave.h"

//#define __Debug_Dump

//=================================================================================================
// 定義・定数・グローバル変数等
//=================================================================================================
using namespace std;

#define	byte	unsigned char
#define	word	unsigned short
#define	sbyte	char
#define	sword	short
#define	ubyte	unsigned char
#define	uword	unsigned short

#define	_BIT0	0x01
#define	_BIT1	0x02
#define	_BIT2	0x04
#define	_BIT3	0x08
#define	_BIT4	0x10
#define	_BIT5	0x20
#define	_BIT6	0x40
#define	_BIT7	0x80

#define	_BIT8	0x0100
#define	_BIT9	0x0200
#define	_BIT10	0x0400
#define	_BIT11	0x0800
#define	_BIT12	0x1000
#define	_BIT13	0x2000
#define	_BIT14	0x4000
#define	_BIT15	0x8000

word endianW(word in)
{
#if 0
	union {
		byte B[2];
		word w;
	}	HL, LH;
	HL.w = in;
	LH.B[0] = HL.B[1];
	LH.B[1] = HL.B[0];
	return LH.w;
#else
	return in;
#endif
}


//=================================================================================================
// Section メモリパレット
//=================================================================================================
color_t Palette[256];
word    Palette_num = 0;
int     Trans_No = -1;

void	ANALOG_SAVE(byte *buffer)
{
	for (int p = 0 ; p < 16 ; p++ ) {
		byte b = *buffer++;
		byte r = *buffer++;
		byte g = *buffer++;
		b = (b|(b<<4));
		r = (r|(r<<4));
		g = (g|(g<<4));

		Palette[p].r = r;
		Palette[p].g = g;
		Palette[p].b = b;
		Palette[p].a = 255;
	}
	Palette_num = 16;
}
byte v2_analog[48] = {
		0x000,0x000,0x000,
		0x00F,0x000,0x000,
		0x000,0x00E,0x000,
		0x008,0x00F,0x007,
		0x002,0x000,0x00B,
		0x00F,0x000,0x00D,
		0x000,0x00F,0x00A,
		0x00F,0x00F,0x00F,

		0x00C,0x008,0x008,
		0x009,0x000,0x000,
		0x000,0x009,0x000,
		0x00B,0x00F,0x00D,
		0x001,0x000,0x008,
		0x006,0x00C,0x008,
		0x003,0x00A,0x005,
		0x002,0x002,0x002
	};

byte realm_analog[48] = {
	0,0,0,
	14,3,0,
	0,14,0,
	8,15,7,
	5,0,9,
	14,0,12,
	0,15,10,
	15,15,15,
	10,9,8,
	8,0,0,
	0,8,0,
	10,15,12,
	0,0,6,
	2,10,5,
	5,13,8,
	2,0,2
	};

byte ragna_analog1[48] = {
	0x00,0x00,0x00,
	0x00,0x05,0x00,
	0x00,0x0B,0x00,
	0x07,0x00,0x00,
	0x0D,0x04,0x04,
	0x0E,0x06,0x0A,
	0x07,0x05,0x07,
	0x05,0x0F,0x0B,
	0x00,0x07,0x05,
	0x04,0x0B,0x07,
	0x09,0x0D,0x0B,
	0x00,0x00,0x05,
	0x00,0x00,0x0A,
	0x0E,0x0E,0x0E,
	0x09,0x09,0x09,
	0x0f,0x0f,0x00
};
byte ragna_analog2[48] = {
	0x00,0x00,0x00,
	0x00,0x05,0x00,
	0x00,0x0B,0x00,
	0x07,0x00,0x00,
	0x0D,0x04,0x04,
	0x0E,0x06,0x0A,
	0x07,0x05,0x07,
	0x05,0x0F,0x0B,
	0x00,0x07,0x05,
	0x04,0x0B,0x07,
	0x09,0x0D,0x0B,
	0x00,0x00,0x05,
	0x00,0x00,0x0A,
	0x0E,0x0E,0x0E,
	0x04,0x0f,0x0e,	/*0x09,0x09,0x09,*/
	0x0f,0x0f,0x0f	/*2,0,2*/		/*0FFH,0FFH,000H*/
};

byte bible2_analog[48] = {
	0x00,0x00,0x00,
	0x0A,0x00,0x04,
	0x00,0x0E,0x00,
	0x07,0x0F,0x08,
	0x02,0x00,0x08,
	0x0E,0x00,0x0C,
	0x00,0x0F,0x0A,
	0x0F,0x0F,0x0F,
	0x09,0x08,0x08,
	0x07,0x00,0x02,
	0x00,0x06,0x00,
	0x0A,0x0F,0x0C,
	0x00,0x00,0x05,
	0x06,0x0C,0x08,
	0x03,0x0A,0x05,
	0x08,0x08,0x08

	/* FACE_
	   0x00,0x00,0x00,
	   0x00,0x07,0x03,
	   0x03,0x0B,0x05,
	   0x03,0x0D,0x07,
	   0x05,0x0D,0x09,
	   0x05,0x0F,0x0B,
	   0x07,0x0F,0x0D,
	   0x09,0x0F,0x0F,
	   0x04,0x04,0x04,
	   0x04,0x04,0x04,
	   0x04,0x04,0x04,
	   0x04,0x04,0x04,
	   0x04,0x04,0x04,
	   0x04,0x04,0x04,
	   0x04,0x04,0x04,
	   0x04,0x04,0x04
	*/
};

//=================================================================================================
// Section PICT クラス
//=================================================================================================
class	Z80
{
	public:
	static	int		_BIT( int n, int reg )
	{
		return ( (reg & (1 << n)) == 0 ? 0 : 1 );
	}
	static	int		_RES( int n, int reg )
	{
		reg &= ~(1 << n);
		return	reg;
	}
	static	int		_SET( int n, int reg )
	{
		reg |= (1 << n);
		return	reg;
	}

	static	int	getAL( byte* bp, int bx )
	{
		int	AL = bp[ bx ];
		return AL & 0x0ff;
	}
	static	int	getAX( byte* bp, int bx )
	{
		int	AL = getAL( bp, bx );
		int	AH = getAL( bp, bx+1 );
		return ((AH<<8)|AL);
	}
};

#define  Work_YY  (80)
#define  Vram_plane		(32768)

	static	int	PSCNT	=	-1;			// 検索長

	static	int	PLANE_GAP	=	0;		// 次の転送先アドレス長 （プレン）
	static	int	WRT_SKIP_Y	=	Work_YY;			// 展開ポインタ移動用
	static	int	WRT_SKIP_Y2	=	Work_YY-1;

	static	int 	PICT_HD[]	= {	0x50, 0x49, 0x43, 0x54 };		// 'PICT'

	static	int	PICT_COUNT;

class	Pict
{
public:
	//------  ＷＲＩＴＥ  -------	WRITE.ASM
	// PICT データユーティリティクラス
	byte*	data_seg;
	int		data_top;
	int		data_cur;

	int		size_x;			// Size_X (CL)
	int		size_y;			// Size_Y (DX)
	int		pict_color;			// Color  (AH)
	int		pict_type;			// Type   (AL)

	// コンストラクタ
	Pict( byte* data, int index )
	{
		data_seg = data;
		data_top = index;
		data_cur = index;
	}
	Pict( const Pict& s )
	{
		data_seg = s.data_seg;
		data_top = s.data_top;
		data_cur = s.data_cur;
		size_x = s.size_x;			// Size_X (CL)
		size_y = s.size_y;			// Size_Y (DX)
		pict_color = s.pict_color;			// Color  (AH)
		pict_type = s.pict_type;			// Type   (AL)
	}

	int	getSizeX()
	{
		return Z80::getAL( data_seg, data_top+5 );
	}
	int	getSizeY()
	{
		int ch = Z80::getAL( data_seg, data_top+6 );
		int ah = ((data_seg[data_top+8] & 0x80) == 0 ? 0 : 256 );	// Type Bit 7  =  Size_Y Bit 8
		return ch+ah;
	}
	int	getColor()
	{
		return Z80::getAL( data_seg, data_top+7 );		// Color  (AH)
	}
	int	getType()
	{
		int cl = data_seg[ data_top+8 ] & 0x7f;
		return cl;
	}
	int	getWriteAddr()
	{
		return Z80::getAX( data_seg, data_top+9 );		// Write Address [DX]
	}
	int	getTime()
	{
		return Z80::getAL( data_seg, data_top+11 );		// Time [AL]
	}

	int	loadWord()
	{
		int	l = loadByte();
		int	h = loadByte();
		int	w = l + ( h << 8 );
		return w;
	}
	int	getByte()
	{
		int	b = Z80::getAL( data_seg, data_cur );

		//System.out.println( "getByte = " + data_cur + ":" + Integer.toHexString(b) );

		return b;
	}
	int	loadByte()
	{
		int	b = getByte();
		data_cur++;
		return b;
	}

	//------    ＷＲＩＴＥ.ＡＳＭ　　------

	//;;==========  Ｐｉｃｔ ヘッダーの検索  (1-)  ===============

	//	Data Addres  BP:[BX]    / Serach Num [AL]

	static	int	PICT_SEARCH( byte* bx, int bx_length, int no )
	{
		int	index =	PICT_SEA_M( bx, bx_length, no );
		if ( index < 0 )
		{
			//ERROR_M( 20 );			// 検出失敗
		}
		return index;
	}

	// 偶数位置にあるヘッダ 'PICT' を検索

	static	int	PICT_SEA_M( byte* bx, int bx_length, int no )
	{
		int	index = 0;
		PICT_COUNT = 0;
		while ( true )
		{
			while ( true )
			{
				// 検索オーバー
				if ( index >= bx_length || index >= 65536 ) {
					return -1;
				}

				// 偶数位置にあるヘッダ 'PICT' を検索
				if ( bx[ index ] == PICT_HD[0] && bx[ index+1 ] == PICT_HD[1] )
				{
					if ( bx[ index+2 ] == PICT_HD[2] && bx[ index+3 ] == PICT_HD[3] )
					{
						PICT_COUNT ++;
						break;
					}
					else
					{
						index += 4;
					}
				}
				else
				{
					index += 2;
				}
			}

			if ( (--no) == 0 )
			{
				break;
			}
			else
			{
				index += 4;
			}
		}
		//SREXT:	MOVX	AX,BX, BX,DI		; Count 残数 [AX]
		return index;			// 指定データ [BX]  (Z)
	}


	//------  読み込んだファイル内の Pict 数を検索  [AX] ------
	static	int	GET_PICT_CNT(byte* bx, int size)
	{
		PICT_SEA_M( bx, size, 1000 );
		return	PICT_COUNT;			// PICT 数  [AX]
	}


	//;;=======  メモリー 表示  Write:  =========

	//	BP:[BX] ---> AX:[DX]

	static	void	WRITE_MEMORY( byte* bp, int bx, byte* ax, int dx )
	{
		//MOVX	TO_MEM,AX, TO_MEM+2,AX	; 書き込みを行う セグメントを設定
		//MOVX	TO_MEM+4,AX, TO_MEM+6,AX

		//MOV	WRT_TABLE,OF TO_MEM	; メモリー 表示		[ WRITE: ]
		Pict*	_pict = new Pict( bp, bx );
		Pict&	pict = *_pict;

		int	wx = pict.getSizeX();		// 座標を得る 
		int	wy = pict.getSizeY();
		PLANE_GAP = wy * wx;		// 次の転送先アドレス長 （プレン）
		WRT_SKIP_Y = wx;			// 展開ポインタ移動用
		WRT_SKIP_Y2 = wx - 1;

		pict.W_SUB( ax, dx );			// 通常 400 ラインの表示  

		//MOV	WRT_TABLE,OF TO_VRAM	; 画面 表示		[ WRITE: ]
		PLANE_GAP = Vram_plane;
		WRT_SKIP_Y = Work_YY;
		WRT_SKIP_Y2 = Work_YY-1;
	}
#if 0
	static	void	WRITE_ANI( byte* bp, int bx, Tos._TO_ANI [] seg, int dx )
	{
		//MOV	WRT_TABLE,OF TO_MEM	; メモリー 表示		[ WRITE: ]
		Pict*	_pict = new Pict( bp, bx );
		Pict&	pict = *_pict;

		PLANE_GAP = seg[1].offset;		// 次の転送先アドレス長 （プレン）
		WRT_SKIP_Y = Work_YY;			// 展開ポインタ移動用
		WRT_SKIP_Y2 = Work_YY - 1;

		pict.W_SUB( seg[0].bank, dx );			// 通常 400 ラインの表示  

		//MOV	WRT_TABLE,OF TO_VRAM	; 画面 表示		[ WRITE: ]
		PLANE_GAP = Vram_plane;
		WRT_SKIP_Y = Work_YY;
		WRT_SKIP_Y2 = Work_YY-1;
	}
#endif

	//;;============  Ｃｕｔ　Ｔｒａｎｓ　==============	（ 圧縮絵の展開 ）

	//-------  通常 400 ラインの表示  --------

	//	BP:[BX] ---> [DX]

	//	画面展開では WRT_TABLE (TO_VRAM) / PLANE_GAP (0000)
	//		     WRT_SKIP_Y (YY)  / WRT_SKIP_Y2 (YY-1) にて使用

	static	void	WRITE( byte* bp, int bx, int dx, byte* vram )
	{
		Pict*	_pict = new Pict( bp, bx );		// ローカル ワーク の確保
		Pict&	pict = *_pict;
		//MOV	AX,SS:WRT_TABLE		; 書き込みを行う セグメントを設定
		//MOV	[BP].ESEG_PNT,AX

		PLANE_GAP = Vram_plane;
		WRT_SKIP_Y = Work_YY;
		WRT_SKIP_Y2 = Work_YY-1;
		pict.W_SUB( vram, dx );			// 表示 サブ   [SI] -> [DI]
	}


	//-----	(SI)-(DI)  / Size X (CL)  / Size Y (DX) /  Col (AH)   ------
	class	PictWrite
	{
	  public:
		virtual	void	W_ENTRY() {}

		int		PL_GAP;			// 次の転送先アドレス長 （プレン）
		int		WX_CNT;			// Ｘカウント

		byte*	es;
		int		di;

		int bitmap_index;		// Bit Map [BX] + Offset Address
		int bitmap_mask;
		int CH;

		Pict*   pict;
		int		data_top;		// Pict より受け取る

		// コンストラクタ
		PictWrite() {}
		PictWrite( Pict* s, byte* dst, int index )
		{
			pict = s;
			data_top = s->data_top;

			es = dst;
			di = index;
		}

		//--------  表示用 メイン  (BX) をエントリーとする  ---------

		//    この Format は Data Top に (Bitmap) Offset Address がある。  [ Word ]

		//-----	[SI]-[DI]  / Size X [CL]  / Size Y [DX] /  Col [AH]   ------
		void	WRITE_()
		{
			PL_GAP = PLANE_GAP - pict->size_x;		// 次の転送先アドレス長 （プレン）

			bitmap_index = this->data_top + pict->loadWord() - 1;		// Bit Map [BX] + Offset Address

			// Hi.Low Flag  / 8 Bit Over Flug 
			CH = 0;
			bitmap_mask = 1;
			int c;

			for ( c = 0 ; c < 4 ; c++ )
			//for ( c = 0 ; c < 1 ; c++ )
			{
				int do_color = (pict->pict_color & 1);			// カラー 
				pict->pict_color >>= 1;
				if ( do_color == 0 ) continue;

				//PUSH	AX			; Color [AH] / SizeX [AL] / SizeY [DX]
				WX_CNT = pict->size_x;		// Ｘカウント	Size_X  [AL]	

				this->W_ENTRY();				// 展開 プログラム アドレス

				di += PL_GAP;			// 画面時 (-X) / Mem 時 (X*(Y-1))
			}
		}

		// 8bit over flag
		// BX =  Bit Map Address
		bool	TestNextBitmap()
		{
			bitmap_mask >>= 1;
			if ( bitmap_mask == 0 ) {
				bitmap_mask = 0x080;
				bitmap_index ++;
			}
			int  bitmap_data = Z80::getAL( pict->data_seg, bitmap_index );
#if defined( __Debug_Dump )
			std::cout <<  "Bitmap[" << std::hex<< bitmap_index << "]:" << bitmap_data << ":" << bitmap_mask <<std::dec << std::endl;
#endif
			return ( (bitmap_data & bitmap_mask) != 0 );
		}
		//------  Get  4 Bit Data  ------
		// Get High 4 bit color index
		int		GETDTL()
		{
			int	dat;
			if ( Z80::_BIT( 0, CH ) != 0 )			// CH =  Hi / Low  Flug
			{
				dat = pict->loadByte();			// Get  Data 8 Bit
				dat <<= 4;
			}
			else
			{
				dat = pict->getByte();
			}
			dat &= 0xf0;			// Set Hi (4 Bit)
			CH ^= 0x01;
			//System.out.println( "DTL = " + Integer.toHexString(dat) );
			return dat;
		}
		// Get Low 4 bit color index
		int		GETDTR()
		{
			int	dat;
			if ( Z80::_BIT( 0, CH ) != 0 )			// CH =  Hi / Low  Flug
			{
				dat = pict->loadByte();			// Get  Data 8 Bit
			}
			else
			{
				dat = pict->getByte();
				dat >>= 4;			// Set Hi (4 Bit)
			}
			dat &= 0x0f;			// Set Low (4 Bit)
			CH ^= 0x01;
			//System.out.println( "DTR = " + Integer.toHexString(dat) );
			return dat;
		}
	};

	//----  （５）  -----	（ 差分 専用 ）

	//------  縦 ( 8*1 ) ビット マップ    ------

	//	Bitmap = CS+8000H (16k)
	//	この Format は Data Top に (Bitmap) Offset Address がある． (Word)
	class	WRITEM	: public PictWrite
	{
	  public:
		WRITEM( Pict* s, byte* dst, int index ) : PictWrite( s, dst, index )
		{
		}

		virtual void	W_ENTRY()
		{
			//[BP].W_ENTRY,OF TRANSM		; 展開 プログラム アドレス
			//----	BX Bitmap  /  CX Cont Flug  /  DX Size (Y)  

			//WYLOPM:	PUSHX	DX,DI
			do
			{
				int count_y = pict->size_y;
				int dii = di;

				//WXLOPM:
				while ( true )
				{
					// 8bit over flag
					// BX =  Bit Map Address
					if ( TestNextBitmap() )
					{
						int	dat = pict->loadByte();			// Get  Data 8 Bit
						int len = pict->loadByte();			// Get  Counter
						do
						{
							dii = WritePictExt( dii, dat );

							--count_y;			// DEC (Y)
							if ( count_y == 0 )
							{
								di++;			// 書き込み アドレス + 1
								--WX_CNT;		// Ｘカウント-1
								if ( WX_CNT == 0 ) return;
								dii = di;
								count_y = pict->size_y;
							}

							len = (--len) & 0x0ff;			// Repeat Count..
						} while ( len != 0 );
					}
					else
					{
						int	dat = pict->loadByte();			// Get  Data 8 Bit

						dii = WritePictExt( dii, dat );
						--count_y;
						if ( count_y == 0 ) break;
					}
				}

				di++;			// 書き込み アドレス + 1
				--WX_CNT;		// Ｘカウント-1
			} while ( WX_CNT != 0 );
		}

		int	WritePictExt( int dptr, int dat )
		{
				es[dptr] ^= dat;
	//		if ( dat != 0 ) {
	//			es[dptr] ^= dat;
	//		}
			dptr += WRT_SKIP_Y;	// Y =  Y + 80
			return dptr;
		}
	};

	//----  （６）  -----

	//------  縦 ( 4*2 ) Bitmap 可変カウント （偶数）   ------

	//	Bitmap = CS+8000H (16k)
	//	この Format は Data Top に (Bitmap) Offset Address がある． (Word)
	//	( [SI] + [SI+80] ) 単位の縦リピート
	class	WRITEW	: public PictWrite
	{
	  public:
		WRITEW( Pict* s, byte* dst, int index ) : PictWrite( s, dst, index )
		{
		}

		virtual void	W_ENTRY()
		{
			//----	BX Bitmap  /  CX Cont Flug  /  DX Size (Y)

			int repeat_length = 0;	// CS:P_CNT
			//TRANS6:MOV	BP,DX
			do
			{
				int count_y = pict->size_y;	// DX
				int AH, AL;

				int diorg = di;
				int din = 0;
				//BIT6_:
				while ( true )
				{
					if (repeat_length == 0) {
						if ( TestNextBitmap() )	//BIT_CHK
						{
							repeat_length = GetRepeatLength();	//MOV	CS:P_CNT,AL
						}
						AH = GETDTL();			// Get  4 Bit Left 
						AL = GETDTL();			// Get  4 Bit Left
#if defined( __Debug_Dump )
						std::cout << "Left: (0x" << std::hex<<AH<<":"<<AL<< std::dec<<")" << std::endl;
#endif
					}
					if (repeat_length != 0) {
						//-----  Repeat  (Left 4 Bit)   ------
#if defined( __Debug_Dump )
						std::cout << " 0x" << std::hex<< (AH|AL>>4) << ".."<< std::dec << repeat_length << std::endl;
#endif
					  //RPLP6:
						do
						{
							di = WritePictLeft( di, AH, AL );
							count_y -= 2;	//SUB	DX,2
							if (count_y == 0) break;	// JZ	COPE6
							din += 2;
						} while (--repeat_length != 0);	//RRMD6:	DEC	CS:P_CNT JNZ	RPLP6
					}
					else
					{
#if defined( __Debug_Dump )
						std::cout << " 0x" << std::hex<< (AH|AL>>4) << "..(1)"<< std::dec << std::endl;
#endif
						din += 2;
						di = WritePictLeft( di, AH, AL );
						count_y -= 2;	//SUB	DX,2
					}
					//COPE6:
					if ( count_y == 0 )
					{
						if (repeat_length != 0) {
							// continue to Right
							if (--repeat_length != 0) {
								AH >>=4;	//SHR	AX,4;
								AL >>=4;
								//JMP	RRMD5;
#if defined( __Debug_Dump )
								std::cout << "ContR: (0x" << std::hex<<AH<<":"<<AL<< std::dec<<")" << std::endl;
#endif
							}
						}
						break;
					}
					// loop to BIT6_
				}

				// DI =  Rast Address
				//------  Trans (Right 4 Bit)  ------
				count_y = pict->size_y;	// MOV	DX,BP
				//BIT62_:
				while ( true )
				{
					if (repeat_length == 0) {
						if ( TestNextBitmap() )	//BIT_CHK
						{
							repeat_length = GetRepeatLength();	//MOV	CS:P_CNT,AL
						}
						AH = GETDTR();			// Get  4 Bit Right 
						AL = GETDTR();			// Get  4 Bit Right 
#if defined( __Debug_Dump )
						std::cout << "Right: (0x" << std::hex<<AH<<":"<<AL<< std::dec<<")" << std::endl;
#endif
					}
					if (repeat_length != 0) {
						//-----  Repeat (Right 4 Bit)   ------
#if defined( __Debug_Dump )
						std::cout << " 0x" << std::hex<< (AH<<4|AL) << ".."<< std::dec << repeat_length << std::endl;
#endif
					  //RPLP62:
						do
						{
							di = WritePictRight( di, AH, AL );
							count_y -= 2;	//SUB	DX,2
							if (count_y == 0) break;	// JZ	COPE62
							din -= 2;
						} while (--repeat_length != 0);	//RRMD5: DEC	CS:P_CNT	JNZ	RPLP62
					}
					else
					{
#if defined( __Debug_Dump )
						std::cout << " 0x" << std::hex<< (AH<<4|AL) << "..(1)"<< std::dec << std::endl;
#endif
						di = WritePictRight( di, AH, AL );
						count_y -= 2;	//SUB	DX,2
						din -= 2;
					}
					//COPE62:
					if ( count_y == 0 )
					{
						if (repeat_length != 0) {
							// continue to Left
							if (--repeat_length != 0) {
								AH <<= 4;	//SHL	AX,4
								AL <<= 4;
								//JMP	RRMD6;
#if defined( __Debug_Dump )
								std::cout << "ContL: (0x" << std::hex<<AH<<":"<<AL<< std::dec<<")" << std::endl;
#endif
							}
						}
						break;
					}
					// loop to BIT62_
				}
				if (diorg != di) {
					std::cout << "Error:" << diorg << "!=" << di << std::endl;
					exit(0);
				} else {
					//std::cout << "Next:" << di << std::endl;
				}
					
			//	System.out.println( "Write " + Integer.toHexString(dptr) + ":=" + Integer.toHexString(dat) );

				di++;	//INC	DI			; Vram + 1
				--WX_CNT;	//DEC	CS:SIZE_WX		; DEC (X)
#if defined( __Debug_Dump )
				std::cout << "Next:" << di << std::endl;
#endif
			} while ( WX_CNT != 0 );	//JNZ	BIT6_
		}
		int	WritePictLeft( int dptr, int ah, int al )
		{
			es[dptr] = (byte)ah;	//MOV	ES:[DI],AH
			es[dptr+WRT_SKIP_Y] = (byte)al;	//MOV	ES:[DI+YY],AL
			dptr += WRT_SKIP_Y*2;		//ADD	DI,YY*2
			return dptr;
		}
		int	WritePictRight( int dptr, int ah, int al )
		{
			dptr -= WRT_SKIP_Y*2;	//SUB	DI,YY*2
			es[dptr] |= (byte)ah;	//OR	ES:[DI],AH
			es[dptr+WRT_SKIP_Y] |= (byte)al;	//OR	ES:[DI+YY],AL
			return dptr;
		}
		// Get Repeat Count H|L or L
		int	GetRepeatLength()
		{
			int len = 0;
			if ( TestNextBitmap() )	//BIT_CHK
			{
				int hi = GETDTL();			// Get  8 Bit Repeat
				len = GETDTR() | hi;	//OR	AL,AH
				if (len == 0) len = 256;	// Zero Safety.
			}
			else
			{
				len = (GETDTR() + 1);			// Get  4 Bit Repeat	INC	AL
			}
			//std::cout << "GetRepeatLength: " << len << std::endl;
			return len;
		}
	};

	//----- （７）  -----

	//------  縦 ( 4*1 ) ビット マップ    ------

	//	注）　大きく連続した物には向かない．  Repeat Max = 15

	//	Bitmap = CS+8000H (16k)
	//	この Format は Data Top に (Bitmap) Offset Address がある． (Word)
	class	WRITE4	: public PictWrite
	{
	  public:
		WRITE4( Pict* s, byte* dst, int index ) : PictWrite( s, dst, index )
		{
		}

		virtual void	W_ENTRY()
		{
			//----	BX Bitmap  /  CX Cont Flug  /  DX Size (Y)

			int repeat_length = 0;	// CS:P_CNT
			//TRANS4:MOV	BP,DX
			do
			{
				int count_y = pict->size_y;	// DX
				int AL;
				while ( true )
				{
					if (repeat_length == 0) {
						AL = GETDTL();			// Get  4 Bit Left 
						if ( TestNextBitmap() )	//BIT_CHK
						{
							repeat_length = (GETDTR() + 1);			// Get  4 Bit Repeat	INC	AL
						}
					}
					if (repeat_length != 0) {
						//-----  Repeat  (Left 4 Bit)   ------
						do
						{
							di = WritePictLeft( di, AL );
							--count_y;
							if (count_y == 0) break;	// JZ	COPE4
						} while (--repeat_length != 0);
					}
					else
					{
						di = WritePictLeft( di, AL );
						--count_y;
					}
					//COPE4:
					if ( count_y == 0 )
					{
						if (repeat_length != 0) {
							// continue to Right
							if (--repeat_length != 0) {
								AL >>=4;
							}
						}
						break;
					}
					// loop to BIT6_
				}

				//------  Trans (Right 4 Bit)  ------
				count_y = pict->size_y;	// MOV	DX,BP
				while ( true )
				{
					if (repeat_length == 0) {
						AL = GETDTR();			// Get  4 Bit Right 
						if ( TestNextBitmap() )	//BIT_CHK
						{
							repeat_length = (GETDTR() + 1);			// Get  4 Bit Repeat	INC	AL
						}
					}
					if (repeat_length != 0) {
						//-----  Repeat (Right 4 Bit)   ------
						do
						{
							di = WritePictRight( di, AL );
							--count_y;
							if (count_y == 0) break;	// JZ	COPE42
						} while (--repeat_length != 0);
					}
					else
					{
						di = WritePictRight( di, AL );
						--count_y;
					}
					//COPE42:
					if ( count_y == 0 )
					{
						if (repeat_length != 0) {
							// continue to Left
							if (--repeat_length != 0) {
								AL <<= 4;
							}
						}
						break;
					}
				}
				di++;	//INC	DI			; Vram + 1
				--WX_CNT;	//DEC	CS:SIZE_WX		; DEC (X)
			} while ( WX_CNT != 0 );	//JNZ	BIT6_
		}
		int	WritePictLeft( int dptr, int al )
		{
			es[dptr] = (byte)al;
			dptr += WRT_SKIP_Y;
			return dptr;
		}
		int	WritePictRight( int dptr, int al )
		{
			dptr -= WRT_SKIP_Y;
			es[dptr] |= (byte)al;
			return dptr;
		}
	};


	//------　（８） ------

	//------  縦 ( 8*1 ) Bitmap 可変カウント    ------

	//	Bitmap = CS+8000H (16k)
	//	この Format は Data Top に (Bitmap) Offset Address がある． (Word)
	class	WRITEV	: public PictWrite
	{
	  public:
		WRITEV( Pict* s, byte* dst, int index ) : PictWrite( s, dst, index )
		{
		}

		virtual void	W_ENTRY()
		{
			//[BP].W_ENTRY,OF TRANSV		; 展開 プログラム アドレス
			//----	BX Bitmap  /  CX Cont Flug  /  DX Size (X,Y)

			//TRANSV:	PUSHX	DX,DI
			do
			{
				int count_y = pict->size_y;
				int dii = di;

			  //BITV_:
				while ( true )
				{
					// 8bit over flag
					// BX =  Bit Map Address
					if ( TestNextBitmap() )
					{
						//TENKV
						//--------  Repeat (V)  --------
						int l = GETDTL();			// Get  4 Bit Left 
						int r = GETDTR();			// Get  4 Bit Right
						int dat = l|r;
						int len;
						if ( TestNextBitmap() )
						{
							//LONGV
							int l2 = GETDTL();			// Get  4 Bit Left 
							int r2 = GETDTR();			// Get  4 Bit Right
							len = (l2|r2);
						}
						else
						{
							len = (GETDTR() + 1);			// Mini Count  4 Bit	Count +1
						}
						do
						{
							dii = WritePict( dii, dat );

							--count_y;			// DEC (Y)
							if ( count_y == 0 )
							{
								di++;			// 書き込み アドレス + 1
								--WX_CNT;		// Ｘカウント-1
								if ( WX_CNT == 0 ) return;
								dii = di;
								count_y = pict->size_y;
							}

							len = (--len) & 0x0ff;			// Repeat Count..
						} while ( len != 0 );
					}
					else
					{
						int l = GETDTL();			// Get  4 Bit Left 
						int r = GETDTR();			// Get  4 Bit Right

						dii = WritePict( dii, l | r );
						--count_y;
						if ( count_y == 0 ) break;
					}
				}

				di++;			// 書き込み アドレス + 1
				--WX_CNT;		// Ｘカウント-1
			} while ( WX_CNT != 0 );
		}

		int	WritePict( int dptr, int dat )
		{
			//if ( dptr < 0 || dptr >= Vram_plane ) {
			//	System.out.println( "Write " + Integer.toHexString(dptr) + ":=" + Integer.toHexString(dat) );
			//}

			es[dptr] = (byte)dat;
			dptr += WRT_SKIP_Y;
			return dptr;
		}

	};


	//------  ベタ 無圧縮    ------

	//-----	[SI]-[DI]  / Size X [CL]  / Size Y [DX] /  Col [AH]   ------
	class	WRITE10	: public PictWrite
	{
	  public:
		WRITE10( Pict* s, byte* dst, int index ) : PictWrite( s, dst, index )
		{
		}

		virtual void	W_ENTRY()
		{
		}
	};

	//--------  ＰＩＣＴ ファイル の 展開表示    DS:[SI] -> [DI]  --------
	void	W_SUB( byte* es, int di )
	{
		//CP	[SI],'IP'		; ＰＩＣＴ の チェック
		//MOV	AL,20			; 検出失敗。
		//JNZ	ERROR_M
		PictWrite*	pict_write = NULL;

		//MOV	BX, = this	SI			; Offset (BX)
		size_x = getSizeX();			// Size_X (CL)
		size_y = getSizeY();			// Size_Y (DX)
		pict_color = getColor();			// Color  (AH)
		pict_type = getType();				// Type   (AL)

		data_cur = this->data_top + 9;
		if ( Z80::_BIT( 6, pict_type ) != 0 )			// Type Bit 6  =  拡張ヘッダー
		{
			data_cur += 7;
		}
		//System.out.println( "SizeX " + size_x );
		//System.out.println( "SizeY " + size_y );
		//System.out.println( "Color " + pict_color );
		//System.out.println( "Type  " + pict_type );
		//System.out.println( "Image " + data_cur );

		switch ( pict_type & 0x3f )
		{
		  case 0:			// ０）- 横 展開
		  case 1:			// １）- 縦 展開
		  case 2:			// ２）-  チップ リピート ( 4*2 *4P ) Bitmap 可変カウント
		  case 3:			// ３）-  ブロック 圧縮 ( 8*2 *4P )
		  case 4:			// ４）-  縦 ( 8*2 ) Bitmap 可変カウント （偶数）
			break;

		  case 5:			// ５）-  縦 ( 8*1 ) ビット マップ  
			pict_write = new WRITEM( this, es, di );
			break;

		  case 6:			// ６）-  縦 ( 4*2 ) Bitmap 可変カウント （偶数） 
			pict_write = new WRITEW( this, es, di );
			break;
		  case 7:			// ７）-  縦 ( 4*1 ) ビット マップ  
			pict_write = new WRITE4( this, es, di );
			break;
		  case 8:			// ８）-  縦 ( 8*1 ) Bitmap 可変カウント  
			pict_write = new WRITEV( this, es, di );
			break;
		  case 9:			// ９）-  縦 ( 4*1 ) Bitmap 可変カウント  
			break;
		  case 10:			// 10）-  ベタ 無圧縮  
		  case 11:			// 11）-  差分  
			//pict_write = new WRITE10( this, es, di );
			//Vram.PUT_PATTRN( data_seg, data_cur, di, pict->size_x, pict->size_y );
			{
				byte*  bp = data_seg;
				int bx = data_cur;
				int dx = di;
				int ch = size_x;
				int cl = size_y;
				int plane, line, c;
				for ( plane = 0 ; plane < Vram_plane*4 ; plane += Vram_plane )
				{
					int dxx = dx;
					for ( line = 0 ; line < cl ; line ++ )	// Size Y  [CL]
					{
						int	di = dxx;	// X Loop Count
						for ( c = 0 ; c < ch ; c++ )			// Size X  [BX]
						{
							es[ plane + di ] = bp[ bx++ ];
							di++;
						}
						dxx += Work_YY;
					}
				}
			}
			return;
		  default:
			  break;
			//MOV	AL,198			; 圧縮未対応
			//JMP	ERROR_M
		}
		if ( pict_write != NULL )
		{
			pict_write->WRITE_();
		}
	}

};

//=================================================================================================
// Section WIN ローダ
//=================================================================================================
static	const int	_plane	=	32768;
byte vram[_plane*4];
byte *plane_b = &vram[0];
byte *plane_r = &vram[_plane];
byte *plane_g = &vram[_plane*2];
byte *plane_h = &vram[_plane*3];

static	unsigned char	dbuf[65536*2];
static	unsigned char	analog_buf[2048];//32+48

class WinLoader
{
	const std::string rootpath;
	const std::string outpath;
	const std::string palpath;
	bool        verbose;
	bool   test_only; //  Screen_Out = ON;
	int  Pict_No;
	int  Pict_sizeX;
	int  Pict_sizeY;
	int  Pict_List[64];
	int  Pict_Count;
	int  Cut_posX;
	int  Cut_posY;
	int  Cut_sizeX;
	int  Cut_sizeY;
	bool  Camel_PAT;
	int  Palette_Type;
	bool  analog_load;
	bool  all_pict;
	int   Trans_Palette;
	bool  use_trans_palette;
	int   Neg_Palette;
	bool  use_neg_palette;

	std::ostream* _ofp;

public:
	WinLoader(hqw::args::Options& options)
		: rootpath( options["-i"] )
		, outpath( options["-o"] )
		, palpath( options["-a"] )
		, verbose( options["-v"].Check() )
		, Palette_Type( options["-p"] )
		//, Pict_List( options["-n"] )	// 
		, Camel_PAT( options["-c"].Check() )
		, analog_load( options["-a"].Check() )
		, test_only( options["-test"].Check() )
		, all_pict( options["-all"].Check() )
		, Trans_Palette( options["-trans"] )
		, use_trans_palette( options["-trans"].Check() )
		, Neg_Palette( options["-neg"] )
		, use_neg_palette( options["-neg"].Check() )
		, _ofp(NULL)
	{
		Pict_No = 0;
		Pict_Count = 0;
		if (options["-n"].Check()) {
			std::vector<int> list = options["-n"];
			for (int n = 0; n < list.size(); n++) {
				Pict_List[n] = list[n];
				Pict_Count++;
				//std::cout << "N:" << list[n] << std::endl;
			}
		}
	}
	~WinLoader() {
		if (_ofp != NULL) delete(_ofp);
	}

	void ScanFile(const std::string filepath)
	{
		// 出力ファイル
		std::string outtag = filepath;
		outtag = std::regex_replace( outtag, std::regex("^.*/"), "");
		outtag = std::regex_replace( outtag, std::regex("\\..*$"), "");
		if (std::regex_match( outpath, std::regex(".*/$"))) {
			std::transform(outtag.begin(), outtag.end(), outtag.begin(), ::tolower);
			outtag = outpath + outtag;
		}

		//  ファイルを開く
		std::string srcfile = rootpath+filepath;
		if (verbose) {
			std::cout << ("Src File "+srcfile) << std::endl;
		}

		// 画像ファイルの読み込み
		int  image_size = 0;
		{
			FILE	*fp;
			if ((fp = fopen(srcfile.c_str(), "rb"))) {
				image_size = fread(dbuf, 1, 65536*2, fp);
				fclose(fp);
			} else {
				exit(0);
			}
		}
		// PICT 数の検索
		int picts = Pict::GET_PICT_CNT(dbuf, image_size);
		if (verbose && !Camel_PAT) {
			std::cout << "Picts " << picts << std::endl;
		}
		if (picts == 0) {
			std::cout << "No Pict Image." << std::endl;
			exit(1);
		}
		if ( Pict_Count == 0 ) {
			if (all_pict) {
				//Pict_Count = picts;
				for (int n = 0; n < picts; n++) Pict_List[n] = (n+1);
			} else {
				Pict_List[0] = 1;
				//Pict_Count = 1;
				picts = 1;
			}
		}
		else {
			picts = Pict_Count;
		}

		std::memset(vram, 0, sizeof(vram));

		// 画像ファイルの展開と表示
		for (int pn = 0 ; pn < picts ; pn++ )//Pict_Count
		{
			Pict_No = Pict_List[ pn ];
			int no  = Pict_No;
			int ptr = Pict::PICT_SEARCH(dbuf, image_size, no);
			if (ptr < 0) {
				std::cout << "Pict No [" << no << "] can't find.." << std::endl;
				exit(1);
			}

			// 画像情報取得
			{
				Pict* pict = new Pict( dbuf, ptr );

				//int sx = *(dbuf + ptr + 5);
				//int sy = *(dbuf + ptr + 6);
				//if ((*(dbuf+ptr+8) & 0x80) != 0) sy += 256;
				//int stype = *(dbuf+ptr+8) & 0x3F;
				int sx = pict->getSizeX();
				int sy = pict->getSizeY();
				int color = pict->getColor();
				int stype = pict->getType();
				if (verbose && !Camel_PAT) {
					std::cout << "No. "<< no <<"  size ("<<sx<<","<<sy<<")" << std::endl;
					std::cout << "        color (" << color <<")" << std::endl;
					std::cout << "        type (0x" << std::hex<<stype<< std::dec<<")" << std::endl;
				}

				if (pn == 0) {
					Pict_sizeX = sx;
					Pict_sizeY = sy;
				}
				Cut_sizeX = sx;
				Cut_sizeY = sy;

				delete pict;
			}

			word disp = 0;
			Cut_posX = 0;
			Cut_posY = 0;
			if ((*(dbuf+ptr+8) & 0x40) != 0) {
				disp += *(dbuf+ptr+9) + *(dbuf+ptr+10)*256;
				int dx = disp % 80;
				int dy = disp / 80;
				if (verbose) { std::cout << "        put ("<<dx<<","<<dy<<")" << std::endl; }
				Cut_posX = dx;
				Cut_posY = dy;
			}

			if (!test_only) {
				bool useExtPal = false;
				if (analog_load) {
					std::string palfile = rootpath+palpath;
					if (verbose) {
						std::cout << ("Pal File "+palfile) << std::endl;
					}
					FILE *fp;
					if ((fp = fopen(palfile.c_str(), "rb"))) {
						fread(analog_buf, 1, 2048, fp); //32+48
						fclose(fp);

						if ( Palette_Type != 0 ) {
							ANALOG_SAVE(analog_buf + (Palette_Type-1)*80 +32);
						}
						else {
							ANALOG_SAVE(analog_buf+32);
						}
						useExtPal = true;
					}
				}
				else if ( Palette_Type != 0 ) {
					switch ( Palette_Type ) {
						case 1:
							ANALOG_SAVE(v2_analog);
							break;
						case 2:
							ANALOG_SAVE(realm_analog);
							break;
						case 31:
							ANALOG_SAVE(ragna_analog1);
							break;
						case 32:
							ANALOG_SAVE(ragna_analog2);
							break;
						case 4:
							ANALOG_SAVE(bible2_analog);
							break;
						default:
							std::cout << "Palette Select No [" << Palette_Type << "] illegal.." << std::endl;
							exit(1);
					}
					useExtPal = true;
				}

				if (Camel_PAT) {
					if (!useExtPal) ANALOG_SAVE(dbuf);
					if (verbose) {
						int sx = *(dbuf+48);
						int sy = *(dbuf+49);
						std::cout << "unit size ("<<sx<<","<<sy<<")" << std::endl;
					}
				} else {
					if (std::strncmp((const char*)dbuf, "Analog Pallette ", 16) == 0) {
						if (verbose) { std::cout << "Analog Pallette Exist" << std::endl; }
						if (!useExtPal) ANALOG_SAVE(dbuf+32);
					}
				}
				if (Palette_num == 0) {
					std::cout << "Palette Not Selected.." << std::endl;
					exit(1);
				}

				if (use_trans_palette) {
					Palette[Trans_Palette].a = 0;
				}
				if (use_neg_palette) {
					//Palette[0].a = 0;
				}

				/* 画像データ展開 */
				Pict::WRITE(dbuf, ptr, disp, vram);
				if (all_pict) {
					SaveVram(outtag,Cut_posX,Cut_posY,Cut_sizeX,Cut_sizeY);
				}
			}
		}
		if (!test_only) {
			if (!all_pict) {
				SaveVram(outtag,0,0,Pict_sizeX,Pict_sizeY);
			}
		}
	}

	void SaveVram(std::string outtag, int hx, int hy, int sx, int sy)
	{
		int wx = sx * 8;
		int wy = sy;
		byte *image = new byte[ wx * wy ];

		byte *src = vram + (hy*80) + hx;
		byte *ip = image;
		for (int y = wy; y != 0; y--) {
			byte *m0 = src;
			byte *m1 = m0+_plane;
			byte *m2 = m1+_plane;
			byte *m3 = m2+_plane;
			for (int x = sx; x != 0; x--) {
				byte d0 = *m0++; 
				byte d1 = *m1++; 
				byte d2 = *m2++; 
				byte d3 = *m3++; 
				int mask = 0x80;
				for (int mask = 0x80; mask != 0; mask>>=1) {
					word pd = 0;
					pd |= ((d0 & mask) != 0 ? 1 : 0);
					pd |= ((d1 & mask) != 0 ? 2 : 0);
					pd |= ((d2 & mask) != 0 ? 4 : 0);
					pd |= ((d3 & mask) != 0 ? 8 : 0);
					if (use_neg_palette) {
						pd = (pd != 0 ? Neg_Palette : 0);
					}
					*ip++ = pd;
				}
			}
			src += 80;
		}

		if (verbose) {
			std::cout << "Size " << wx << "," << wy << std::endl;
		}

		pixcel_t** pngw = new pixcel_t*[wy];
		for (int y = 0; y < wy; y++) {
			pixcel_t* row = new pixcel_t[wx];
			for (int x = 0; x < wx; x++) {
				row[x].i = image[y*wx+x];
			}
			pngw[y] = row;
		}

		image_t image_work;
		image_work.width = wx;
		image_work.height = wy;
		image_work.color_type = COLOR_TYPE_INDEX;  /**< 色表現の種別 */
		image_work.palette_num = Palette_num; /**< カラーパレットの数 */
		image_work.palette = Palette;     /**< カラーパレットへのポインタ */
		image_work.map = pngw;       /**< 画像データ */

		std::string outfile = outtag + "-" + std::to_string(Pict_No) + ".png";
		if (verbose) {
			std::cout << ("Out File "+outfile) << std::endl;
		}
		write_png_file(outfile.c_str(), &image_work);

		for (int y = 0; y < wy; y++) {
			delete pngw[y];
		}
		delete[] pngw;
		delete[] image;
	}
};

//=================================================================================================
// Section メインエントリー
//=================================================================================================
int main(int argc, char* argv[])
{
	if (argc < 2) {
		std::cout << "win2png [options] win-file" << std::endl;
		std::cout << " WIN PNG 変換 ツール" << std::endl;
		std::cout << " options" << std::endl;
		std::cout << "  -o path : 出力パス指定 result out path (ex. -o ../ )" << std::endl;
		std::cout << "  -i path : ソースパス指定 script root path ( ex. -i ../script/ )" << std::endl;
		std::cout << "  -c      : Camel Pat mode (.PATファイルの強制指定)" << std::endl;
		std::cout << "  -a path : Analog Load (パレットファイルをロード)" << std::endl;
		std::cout << "  -p <n>  : n=1:V2 Analog, 2:Realm , 31|32:Ragna, 4:Bible2" << std::endl;
		std::cout << "  -test   : File Test & Info" << std::endl;
		std::cout << "  -n <n>[,<n>,,] Pict 番号の指定 {1-}" << std::endl;
		std::cout << "  -all    : 全 Pict" << std::endl;
		std::cout << "  -trans <n> : 透過パレット指定 {0-15}" << std::endl;
		std::cout << "  -neg <n>   : ネガ指定 表示パレット{0-15}" << std::endl;
		//std::cout << "    -d<form> BMPファイル名の番号部分のフォーマット指定 (defaultは%s)", OutNo_Default}
		//std::cout << "  -b      : バッチファイル出力" },
		std::cout << "  -v      : Verbose" << std::endl;
		std::cout << " ex)" << std::endl;
		std::cout << "  win2png xxx.img -t xxx.img の画像情報を表示" << std::endl;
		std::cout << "  win2png xxx.img  xxx.img を xxx.png に変換" << std::endl;
		std::cout << "  win2png xxx.img -n 2  xxx.img の二番目を xxx2.png に変換" << std::endl;
		std::cout << "  win2png xxx.img -n 2,3,4  xxx.img の2,3,4番目を表示・合成したものを xxx4.png に変換" << std::endl;
		std::cout << "  win2png xxx.img -p 2 .. イメージをRealmパレットで表示" << std::endl;
		return 1;
	}

	std::vector<std::string> filelist;
	hqw::args::Options options;
	std::vector<int> pict_list;

	options["-v"] = false;  // Verbose
	options["-i"] = "";  // Load Path
	options["-o"] = "";  // Out Path

	options["-a"] = "";  // Palette
	options["-p"] = 0;  // Palette 
	options["-trans"] = -1;  // Palette 
	options["-neg"] = -1;  // Palette 
	options["-c"] = false;  // Camel PAT
	options["-test"] = false;  // Test
	options["-n"] = pict_list;  // Pict Array
	options["-all"] = false;  // All Pict

	hqw::args::Analyzer(argc, argv, filelist, options);

	WinLoader winloader(options);
	for (std::vector<std::string>::iterator itr = filelist.begin(); itr != filelist.end(); ++itr) {
		std::string filepath = (*itr);
		winloader.ScanFile(filepath);
	}

    return 0;
}
